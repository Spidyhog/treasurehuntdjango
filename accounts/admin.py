from django.contrib import admin
from .models import Activation, Participants,Question,Soln,Hint

admin.site.register(Activation)

admin.site.register(Participants)
admin.site.register(Question)
admin.site.register(Soln)
admin.site.register(Hint)