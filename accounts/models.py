from django.db import models
from django.contrib.auth.models import User


class Activation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    code = models.CharField(max_length=20, unique=True)
    email = models.EmailField(blank=True)

class Participants(models.Model):
	score=models.IntegerField(default=0)
	user=models.OneToOneField(User,on_delete=models.CASCADE)
	added=models.DateTimeField(auto_now_add=True)

class Question(models.Model):
	question=models.TextField(default='''<img class="card-img-top" src="..." alt="Card image cap">''')
	answer=models.TextField()
	
	def __str__(self):
	    return str(self.question) +"   "+str(self.answer)

class Soln(models.Model):
	user=models.ForeignKey(User,on_delete=models.CASCADE)
	answer=models.TextField()		
	added=models.DateTimeField(auto_now_add=True)

class Hint(models.Model):
	title=models.CharField(max_length=50)
	body=models.TextField()
	added=models.DateTimeField(auto_now_add=True)
